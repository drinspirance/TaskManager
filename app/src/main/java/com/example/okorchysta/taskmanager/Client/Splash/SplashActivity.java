package com.example.okorchysta.taskmanager.Client.Splash;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.example.okorchysta.taskmanager.Client.Main.MainActivity;
import com.example.okorchysta.taskmanager.Client.Splash.view.ISplashView;

/**
 * Created by OKorchysta on 22.08.2017.
 */

public class SplashActivity extends AppCompatActivity implements ISplashView{

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
